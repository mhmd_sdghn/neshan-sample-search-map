import DriveEtaIcon from '@material-ui/icons/DriveEta';

import { Button } from '@material-ui/core';
import { useContext } from 'react';
import style from '../style/Route.module.css';
import { DataContext } from '../context/data.context';

const Route = ({ routes }) => {
  const DC = useContext(DataContext);
  const { dispatch } = DC;

  return (
    <div className={style.root}>
      {routes.map((route) => (
        <Button
          key={Date.now() + Math.round()}
          onClick={() =>
            dispatch({
              type: 'setState',
              data: {
                steps: route.legs[0].steps,
                selectedRoute: {
                  summary: route.legs[0].summary,
                  distance: route.legs[0].distance.text,
                  duration: route.legs[0].duration.text,
                },
              },
            })
          }
          className={style.btn}
        >
          <div className={style.routeBox}>
            <div className={style.title}>
              <DriveEtaIcon style={{ color: 'grey' }} />
              <strong>
                از مسیر
                {route.legs[0].summary}
              </strong>
            </div>
            <div className={style.info}>
              <span style={{ color: '#188038' }}>
                {route.legs[0].duration.text}
              </span>
              <span style={{ color: 'grey', fontSize: '0.7rem' }}>
                {route.legs[0].distance.text}
              </span>
            </div>
          </div>
        </Button>
      ))}
    </div>
  );
};

export default Route;
