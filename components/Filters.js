import { Select, MenuItem, InputLabel, FormControl } from '@material-ui/core';

const Filters = () => {
  return (
    <>
      <FormControl
        variant="outlined"
        size="small"
        style={{ borderRadius: '50px', width: '90px' , transform: 'scale(0.8)' }}
      >
        <InputLabel id="demo-simple-select-outlined-label">امتیاز</InputLabel>
        <Select
          labelId="demo-simple-select-outlined-label"
          label="امتیاز"
          style={{ borderRadius: '50px'}}
        >
          <MenuItem value={10}>1 امتیاز</MenuItem>
          <MenuItem value={20}>2 امتیاز</MenuItem>
          <MenuItem value={30}>3 امتیاز</MenuItem>
          <MenuItem value={30}>4 امتیاز</MenuItem>
          <MenuItem value={30}>5 امتیاز</MenuItem>
        </Select>
      </FormControl>
      <FormControl
        variant="outlined"
        size="small"
        style={{ borderRadius: '50px', width: '120px' , transform: 'scale(0.8)' , marginRight: '-15px' }}
      >
        <InputLabel id="demo-simple-select-outlined-label">ساعت</InputLabel>
        <Select
          labelId="demo-simple-select-outlined-label"
          label="ساعت"
          style={{ borderRadius: '50px' }}
        >
          <MenuItem value={10}>ساعت دو</MenuItem>
          <MenuItem value={20}>ساعت یک</MenuItem>
        </Select>
      </FormControl>
    </>
  );
};

export default Filters;
