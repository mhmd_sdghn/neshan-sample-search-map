import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import WorkOutlineOutlinedIcon from '@material-ui/icons/WorkOutlineOutlined';
import style from '../style/PrivateLocation.module.css';

const PrivateLocation = () => {
  return (
    <div className={style.root}>
      <div className={style.box}>
        <HomeOutlinedIcon
          style={{
            color: '#1A73E8',
            backgroundColor: '#e8f0fe',
            borderRadius: '50px',
            padding: '8px',
          }}
        />
        <div className={style.info}>
          <span>خانه</span>
          <span>تنظیم کنید</span>
        </div>
      </div>
      <div className={style.box}>
        <WorkOutlineOutlinedIcon
          style={{
            color: '#1A73E8',
            backgroundColor: '#e8f0fe',
            borderRadius: '50px',
            padding: '8px',
          }}
        />
        <div className={style.info}>
          <span>محل کار</span>
          <span>تنظیم کنید</span>
        </div>
      </div>
    </div>
  );
};

export default PrivateLocation;
