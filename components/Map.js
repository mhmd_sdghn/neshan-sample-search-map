/* eslint-disable radix */
/* eslint-disable no-alert */
import { useEffect, useContext, useState } from 'react';
import { useRouter } from 'next/router';
import parser from '@mapbox/polyline';
import mapboxgl from '../mapbox-gl-js/dist/mapbox-gl';
import { DataContext } from '../context/data.context';
import useAxios from '../hooks/useAxios';
import tileUrlGenerator from '../services/tileUrlGenerator.service';
import decodeTile from '../services/decodeTile.service';

// global varibales
let map = null;
let marker = null;
let originMarker = null;
let destinationMarker = null;
const RTLSource =
  'https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-rtl-text/v0.2.0/mapbox-gl-rtl-text.js';

const Map = () => {
  // state
  const router = useRouter();
  const DC = useContext(DataContext);
  const { dispatch, state } = DC;
  const api = useAxios();
  const [center, setCenter] = useState();

  // decode polyline on map
  const decodePolyline = (route) => {
    const decode = parser.toGeoJSON(route.overview_polyline.points);
    dispatch({ type: 'setRouteLine', data: decode });
  };

  // routing
  const Routing = async () => {
    try {
      const res = await api.GET('/v3/direction', {
        type: 'car',
        origin: `${state.origin.location.y},${state.origin.location.x}`,
        destination: `${state.destination.location.y},${state.destination.location.x}`,
      });
      setCenter(res.data.routes[0].legs[0].steps[0].start_location);
      decodePolyline(res.data.routes[0]);
      dispatch({ type: 'setRoutes', data: res.data.routes });
    } catch (err) {
      console.log(err);
      alert('خطا در مسیر یابی');
    }
  };

  // convert point to address
  const pointToAddress = async (lat, lng) => {
    try {
      dispatch({ type: 'setLoading', data: true });
      const res = await api.GET('/v2/reverse', {
        lat,
        lng,
      });
      dispatch({ type: 'setLoading', data: false });
      return {
        title:
          res.data.place || res.data.route_name || res.data.formatted_address,
        location: {
          x: lng,
          y: lat,
        },
      };
    } catch (err) {
      alert('خطا در ارتباط با سرور');
      return console.log(err);
    }
  };

  // draw line
  const drawLine = () => {
    map.addSource('route', {
      type: 'geojson',
      data: {
        type: 'Feature',
        properties: {},
        geometry: { ...state.routeLine },
      },
    });
    map.addLayer({
      id: 'route',
      type: 'line',
      source: 'route',
      layout: {
        'line-join': 'round',
        'line-cap': 'round',
      },
      paint: {
        'line-color': '#1A73E8',
        'line-width': 8,
      },
    });
  };

  // init
  useEffect(() => {
    // Enabel RTL plugin
    // mapboxgl.setRTLTextPlugin(RTLSource);

    // set mapbox token
    mapboxgl.accessToken = process.env.NEXT_PUBLIC_MAPBOX_TOKEN;

    // create marker
    originMarker = new mapboxgl.Marker();
    destinationMarker = new mapboxgl.Marker();

    // create map
    map = new mapboxgl.Map({
      zoom: 14,
      center: [59.55649, 36.28135],
      transformRequest(url) {
        const temp = url.split('/');
        const newUrl = tileUrlGenerator(
          parseInt(temp[6]),
          parseInt(temp[7]),
          parseInt(temp[5]),
          url
        );
        return {
          url: newUrl,
          headers: { secret: 'dngWfFuG2Cm' },
        };
      },

      container: 'hero',
      // style: process.env.NEXT_PUBLIC_MAPBOX_STYLE,
      style: {
        version: 8,
        name: 'Neshan Base Map',
        sources: {
          baseMap: {
            type: 'vector',
            tiles: [
              'https://vts1.neshanmap.ir/basemap/v2/{z}/{x}/{y}/navigator',
            ],
            minzoom: 1,
            maxzoom: 14,
          },
        },
        layers: [
          {
            id: 'street',
            type: 'line',
            source: 'baseMap',
            'source-layer': 'street',
            layout: {
              'line-cap': 'round',
              'line-join': 'round',
            },
            paint: {
              'line-opacity': 0.6,
              'line-color': 'rgb(53, 175, 109)',
              'line-width': 2,
            },
          },
        ],
      },
    });

    map.flyTo({
      center: [59.55649, 36.28135],
      // center: [-122.486052, 37.830348],
      zoom: 12,
      speed: 2,
      curve: 1,
      easing(t) {
        return t;
      },
    });
  }, []);

  // handle click on map
  useEffect(() => {
    if (state.directionMode) {
      map.on('click', async (e) => {
        // when user clicked on map this function get lat lng and set it as origin or destination
        const location = await pointToAddress(e.lngLat.lat, e.lngLat.lng);
        dispatch({ type: 'setRouteInfo', data: location });
      });
    }
  }, [state.directionMode]);

  // fly to selected Location
  useEffect(() => {
    if (router.query.location) {
      marker = new mapboxgl.Marker();
      map.flyTo({
        center: [router.query.lon, router.query.lat],
        zoom: 15,
        speed: 2,
        curve: 1,
        easing(t) {
          return t;
        },
      });
      marker.setLngLat([router.query.lon, router.query.lat]).addTo(map);
    } else if (marker) marker.remove();
  }, [router.query.location]);

  // change map center
  useEffect(() => {
    map.on('move', (e) => {
      // map.getSource('baseMap')

      // console.log(map.getBounds());

      const mapCenter = map.getCenter();
      dispatch({
        type: 'setMapCenter',
        data: { lng: mapCenter.lng, lat: mapCenter.lat },
      });
    });
  }, []);

  // draw route line
  useEffect(() => {
    if (map.loaded()) {
      if (state.routeLine) {
        try {
          map.flyTo({
            center: [...center],
            zoom: 15,
            speed: 2,
            curve: 1,
            easing(t) {
              return t;
            },
          });
          drawLine();
        } catch (err) {
          map.removeLayer('route');
          map.removeSource('route');
          drawLine();
        }
      } else if (!state.routeLine) {
        map.removeLayer('route');
        map.removeSource('route');
      }
    }
  }, [state.routeLine]);

  // watch for selected origin and destination for routing
  useEffect(() => {
    if (state.origin?.location && state.destination?.location) {
      // handle destination marker
      destinationMarker.remove();
      destinationMarker
        .setLngLat([
          state.destination?.location.x,
          state.destination?.location.y,
        ])
        .addTo(map);
      // start routing request
      Routing();
    } else if (state.origin?.location) {
      // handle origin marker
      originMarker.remove();
      originMarker
        .setLngLat([state.origin?.location.x, state.origin?.location.y])
        .addTo(map);
    } else {
      originMarker.remove();
      destinationMarker.remove();
    }
  }, [state.origin, state.destination]);

  return (
    <>
      <div id="hero" />

      <style jsx>
        {`
          #hero {
            width: 100%;
            height: 100vh;
          }
        `}
      </style>
    </>
  );
};

export default Map;
