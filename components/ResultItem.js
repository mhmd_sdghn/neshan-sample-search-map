// import { Room as RoomIcon } from '@material-ui/icons/';
import { Button } from '@material-ui/core';
import Rating from '@material-ui/lab/Rating';
import { useContext } from 'react';
import { useRouter } from 'next/router';
import { DataContext } from '../context/data.context';
import style from '../style/ResultItem.module.css';

const ResultItem = ({ data }) => {
  const { region, title } = data;
  const DC = useContext(DataContext);
  const { dispatch } = DC;
  const router = useRouter();

  const handleClick = async () => {
    // set selected location
    dispatch({ type: 'setSelectedLocation', data });

    router.push(
      `/?location=true&lon=${data.location.x}&lat=${data.location.y}`
    );
  };

  return (
    <Button onClick={handleClick}>
      <div className={style.root}>
        <div className={style.info}>
          <strong>{region}</strong>
          <div className={style.rate}>
            <span>2</span>
            <Rating size="small" value={2} />
            <span>(20)</span>
          </div>
          <span>{title}</span>
          <span>ساعت کار تا 23:00</span>
        </div>
        <div className={style.imageBox}>
          <img
            src="https://www.ajactraining.org/wp-content/uploads/2019/09/image-placeholder.jpg"
            alt="serach result"
          />
        </div>
      </div>
    </Button>
  );
};

export default ResultItem;
