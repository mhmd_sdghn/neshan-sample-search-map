import { IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  Directions as DirectionsIcon,
  TurnedInNot as TurnedInNotIcon,
  MobileScreenShareOutlined as MobileScreenShareOutlinedIcon,
  LocationOnOutlined as LocationOnOutlinedIcon,
  ShareOutlined as ShareOutlinedIcon,
} from '@material-ui/icons';

// style
const useStyles = makeStyles((theme) => ({
  direction: {
    backgroundColor: '#1A73E8 ',
    color: '#fff !important',
  },
  btn: {
    color: '#1A73E8 ',
    border: '1px solid #1A73E8 ',
    padding: '8px'
  },
  btnBox: {
    display: 'flex',
    alignItems: 'stretch',
    justifyContent: 'space-around',
    margin: '20px auto',
  },
  btnItem: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
  },
  subText: {
    fontSize: '0.8rem',
    color: '#1A73E8 ',
    marginTop: '5px',
  },
}));

const BtnBox = () => {
  const classes = useStyles();

  return (
    <div className={classes.btnBox}>
      <div className={classes.btnItem}>
        <IconButton className={classes.btn} size="small">
          <ShareOutlinedIcon />
        </IconButton>
        <span className={classes.subText}>اشتراک</span>
      </div>
      <div className={classes.btnItem}>
        <IconButton className={classes.btn} size="small">
          <MobileScreenShareOutlinedIcon />
        </IconButton>
        <span className={classes.subText}>
          ارسال
          <br />
          به موبایلتان
        </span>
      </div>
      <div className={classes.btnItem}>
        <IconButton className={classes.btn} size="small">
          <LocationOnOutlinedIcon />
        </IconButton>
        <span className={classes.subText}>اطراف</span>
      </div>
      <div className={classes.btnItem}>
        <IconButton className={classes.btn} size="small">
          <TurnedInNotIcon />
        </IconButton>
        <span className={classes.subText}>ذخیره</span>
      </div>
      <div className={classes.btnItem}>
        <IconButton className={[classes.direction, classes.btn].join(' ')} size="small">
          <DirectionsIcon />
        </IconButton>
        <span className={classes.subText}>مسیریابی</span>
      </div>
    </div>
  );
};

export default BtnBox;
