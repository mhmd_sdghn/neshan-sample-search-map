import RoomIcon from '@material-ui/icons/Room';
import { useContext, useState, useEffect } from 'react';
import { Button } from '@material-ui/core';
import { DataContext } from '../context/data.context';
import useAxios from '../hooks/useAxios';
import style from '../style/DirectionResult.module.css';

const DirectionResult = () => {
  // init context
  const DC = useContext(DataContext);

  const { state, dispatch } = DC;
  // axios
  const api = useAxios();

  // state
  const [result, setResult] = useState([]);
  const [inputType, setInputType] = useState('origin');

  // set selected item into context
  const handleClick = (item) => {
    if (inputType === 'origin') dispatch({ type: 'setOrigin', data: item });
    else dispatch({ type: 'setDestination', data: item });
    setResult([]);
  };

  // search input value
  const search = async (type) => {
    try {
      const res = await api.GET(
        `${process.env.NEXT_PUBLIC_NESHAN_API}/v1/search?term`,
        {
          term: type === 'origin' ? DC.state.origin : DC.state.destination,
          lat:
            DC.state?.userPosition?.coords?.latitude ||
            state?.mapCenter?.lat ||
            0,
          lng:
            DC.state?.userPosition?.coords?.longitude ||
            state?.mapCenter?.lng ||
            0,
        }
      );
      setInputType(type);
      setResult(res.data.items);
    } catch (err) {
      alert('خطایی رخ داده');
      console.log(err);
    }
  };

  // watch for user input (origin)
  useEffect(() => {
    if (DC.state.origin && DC.state.origin.length > 2) search('origin');
  }, [DC.state.origin]);

  // watch for user input (destination)
  useEffect(() => {
    if (DC.state.destination && DC.state.destination.length > 2)
      search('destination');
  }, [DC.state.destination]);

  return (
    <>
      {result && result.length ? (
        <div className={style.root}>
          {result.map((item) => (
            <Button
              key={item.title + item.address}
              onClick={() => handleClick(item)}
              style={{ width: '100%' }}
            >
              <div className={style.box}>
                <RoomIcon />
                <span>
                  {item.title},{item.address}
                </span>
              </div>
            </Button>
          ))}
        </div>
      ) : (
        ''
      )}
    </>
  );
};

export default DirectionResult;
