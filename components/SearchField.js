import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import {
  Directions as DirectionsIcon,
  CloseOutlined as CloseOutlinedIcon,
  History as HistoryIcon,
} from '@material-ui/icons/';
import { useRouter } from 'next/router';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 370,
    margin: '10px auto',
    position: 'sticky',
    top: '10px',
    zIndex: 1000,
    boxShadow: 'none !important',
    border: '1px solid #eeeeee',
    borderRadius: '10px',
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
  box: {
    backgroundColor: '#fff',
    // borderBottom: '2px solid #eeeeee',
    marginBottom: 10,
  },
  recent: {
    display: 'flex',
    alignItems: 'center',
    width: 350,
    margin: '20px auto',
  },
}));

export default function SearchField({ q, setQ, setDirectionMode }) {
  const classes = useStyles();
  const router = useRouter();

  const handleChange = (value) => {
    if (router.query.location) router.push('/');
    setQ(value);
  };

  const handleClose = () => {
    if (router.query.location) router.push('/');
    else setQ('')
  };

  return (
    <Paper className={classes.root}>
      <IconButton className={classes.iconButton} aria-label="menu">
        <MenuIcon />
      </IconButton>
      <InputBase
        className={classes.input}
        placeholder="جستجو در  نقشه نشان"
        inputProps={{ 'aria-label': 'جستجو در  نقشه نشان' }}
        value={q}
        onChange={(e) => handleChange(e.target.value)}
      />
      <IconButton
        type="submit"
        className={classes.iconButton}
        aria-label="search"
      >
        <SearchIcon />
      </IconButton>
      <Divider className={classes.divider} orientation="vertical" />
      {q && q.length ? (
        <IconButton className={classes.iconButton} onClick={handleClose}>
          <CloseOutlinedIcon />
        </IconButton>
      ) : (
        <IconButton
          style={{ color: '#1A73E8' }}
          className={classes.iconButton}
          aria-label="directions"
          onClick={() => setDirectionMode(true)}
        >
          <DirectionsIcon />
        </IconButton>
      )}
    </Paper>
  );
}
