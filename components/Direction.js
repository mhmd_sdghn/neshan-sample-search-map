import {
  MenuOutlined as MenuOutlinedIcon,
  DirectionsOutlined as DirectionsOutlinedIcon,
  DirectionsCarOutlined as DirectionsCarOutlinedIcon,
  DirectionsRailwayOutlined as DirectionsRailwayOutlinedIcon,
  DirectionsWalkOutlined as DirectionsWalkOutlinedIcon,
  DirectionsBikeOutlined as DirectionsBikeOutlinedIcon,
  CloseOutlined as CloseOutlinedIcon,
  FiberManualRecordOutlined as FiberManualRecordOutlinedIcon,
  Search as SearchIcon,
  MoreVertOutlined as MoreVertOutlinedIcon,
  Room as RoomIcon,
  SwapVert as SwapVertIcon,
} from '@material-ui/icons';
import { useContext, useState } from 'react';
import { IconButton, CircularProgress } from '@material-ui/core';
import { DataContext } from '../context/data.context';
import style from '../style/Direction.module.css';

const Direction = () => {
  const DC = useContext(DataContext);
  const { dispatch } = DC;

  const handleCloseDirection = () => {
    dispatch({
      type: 'setState',
      data: {
        ...DC.state,
        routeLine: '',
        routes: [],
        steps: [],
        directionMode: false,
        origin: '',
        destination: '',
      },
    });
  };

  return (
    <div className={style.root}>
      <div className={style.head}>
        <div className={style.btnBox}>
          <IconButton>
            <MenuOutlinedIcon style={{ color: '#fff' }} />
          </IconButton>
          <IconButton style={{ backgroundColor: '#651FFF' }}>
            <DirectionsOutlinedIcon style={{ color: '#fff' }} />
          </IconButton>
          <IconButton>
            <DirectionsCarOutlinedIcon style={{ color: '#C5CAE9' }} />
          </IconButton>
          <IconButton>
            <DirectionsRailwayOutlinedIcon style={{ color: '#C5CAE9' }} />
          </IconButton>
          <IconButton>
            <DirectionsWalkOutlinedIcon style={{ color: '#C5CAE9' }} />
          </IconButton>
          <IconButton>
            <DirectionsBikeOutlinedIcon style={{ color: '#C5CAE9' }} />
          </IconButton>
        </div>
        <div>
          <IconButton onClick={() => handleCloseDirection()}>
            <CloseOutlinedIcon style={{ color: '#fff' }} />
          </IconButton>
        </div>
      </div>
      <div className={style.box}>
        <div className={style.iconBox}>
          <FiberManualRecordOutlinedIcon fontSize="small" />
          <MoreVertOutlinedIcon style={{ color: '#C5CAE9' }} />
          <RoomIcon fontSize="small" />
        </div>
        <div>
          <div className={style.inputBox}>
            <input
              autoFocus
              onFocus={() => dispatch({ type: 'setFocus', data: 'origin' })}
              placeholder="مبدا را جستجو کنید"
              className={style.input}
              value={DC.state?.origin?.title || DC.state.origin}
              onChange={(e) =>
                dispatch({ type: 'setOrigin', data: e.target.value })
              }
            />
            {DC.state?.focus === 'origin' && DC.state.loading ? (
              <CircularProgress style={{ color: '#fff' }} size={20} />
            ) : (
              ''
            )}
            {DC.state?.focus === 'origin' && !DC.state.loading ? (
              <SearchIcon />
            ) : (
              ''
            )}
          </div>
          <div className={style.inputBox}>
            <input
              onFocus={() =>
                dispatch({ type: 'setFocus', data: 'destination' })
              }
              placeholder="مقصد را جستجو کنید"
              value={DC.state?.destination?.title || DC.state.destination}
              onChange={(e) =>
                dispatch({ type: 'setDestination', data: e.target.value })
              }
              className={style.input}
            />
            {DC.state?.focus === 'destination' && DC.state.loading ? (
              <CircularProgress style={{ color: '#fff' }} size={20} />
            ) : (
              ''
            )}
            {DC.state?.focus === 'destination' && !DC.state.loading ? (
              <SearchIcon />
            ) : (
              ''
            )}
          </div>
        </div>
        <div className={style.swapBox}>
          <SwapVertIcon style={{ color: '#C5CAE9' }} />
        </div>
      </div>
    </div>
  );
};

export default Direction;
