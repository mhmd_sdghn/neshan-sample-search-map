import Item from './ResultItem';
import Filters from './Filters';

const ResultBox = ({ items }) => {
  return (
    <div>
      {items.length ? (
        <header style={{ width: '370px' }}>
          <Filters />
        </header>
      ) : (
        ''
      )}
      <main>
        {items.map((item) => (
          <Item key={item.region + item.title} data={item} />
        ))}
      </main>
    </div>
  );
};

export default ResultBox;
