import { Skeleton } from '@material-ui/lab';

const SearchLoding = () => {
  return (
    <>
      <Skeleton width={350} height={150} style={{ margin: 'auto' }} />
      <Skeleton width={350} height={150} style={{ margin: 'auto' }} />
      <Skeleton width={350} height={150} style={{ margin: 'auto' }} />
    </>
  );
};

export default SearchLoding;
