import { useContext } from 'react';
import Axios from 'axios';

const fetchAxios = () => {
  // stuff
  const axios = Axios.create({
    baseURL: `${process.env.NEXT_PUBLIC_NESHAN_API}`,
    headers: {
      'Api-Key': `${process.env.NEXT_PUBLIC_NESHAN_API_KEY}`,
    },
  });

  // error handler
  const handleError = (err, method, url) => {
    // handle server errors
    if (err.response) {
      if (process.env.NEXT_PUBLIC_MODE == 'DEVELOP') {
        console.log(
          `${method} request to ${url} failed with status ${err.response.status}`
        );
        // console.log('DEVELOPER MESSAGE ', err.response.data.devMSG)
      }
      return {
        error: true,
        message: err.response.data.userMSG || 'خطا در انجام عملیات',
      };
    }
    // handle unknown errors
    if (err.message && err.message === 'Network Error')
      return {
        error: true,
        message: 'لطفا اتصال اینترنت را بررسی کنید',
      };
    return {
      error: true,
      message: 'خطایی رخ داده ، لطفا با پشتیبانی تماس بگیرید',
    };
  };

  // methods
  const GET = async (url, params) => {
    try {
      const res = await axios({
        method: 'GET',
        url,
        params,
      });
      return res;
    } catch (err) {
      return handleError(err, 'GET', url);
    }
  };

  const POST = async (url, data, params) => {
    try {
      const res = await axios({
        method: 'POST',
        data,
        url,
        params,
      });
      return res;
    } catch (err) {
      return handleError(err, 'POST', url);
    }
  };

  const PUT = async (url, data, params) => {
    try {
      const res = await axios({
        method: 'PUT',
        url,
        data,
        params,
      });
      return res;
    } catch (err) {
      return handleError(err, 'PUT', url);
    }
  };

  const PATCH = async (url, data, params) => {
    try {
      const res = await axios({
        method: 'PATCH',
        url,
        data,
        params,
      });
      return res;
    } catch (err) {
      return handleError(err, 'PATCH', url);
    }
  };

  const DELETE = async (url, params) => {
    try {
      const res = await axios({
        method: 'DELETE',
        url,
        params,
      });
      return res;
    } catch (err) {
      return handleError(err, 'DELETE', url);
    }
  };

  return {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE,
  };
};
export default fetchAxios;
