import '../style/global.css';
import { create } from 'jss';
import rtl from 'jss-rtl';
import {
  StylesProvider,
  jssPreset,
  createMuiTheme,
  ThemeProvider,
} from '@material-ui/core/styles';
import DataContextProvider from '../context/data.context';
import { blue } from '@material-ui/core/colors';

// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

const theme = createMuiTheme({
  direction: 'rtl',
  palette: {
    primary: {
      main: blue[900],
      contrastText: '#fff',
    },
    secondary: {
      main: '#fff',
      contrastText: blue[900],
    },
  },
  typography: {
    fontFamily: ['Yekan', 'iranSans', 'Tahoma'].join(','),
  },
});

const MyApp = ({ Component, pageProps }) => {
  return (
    <DataContextProvider>
      <StylesProvider jss={jss}>
        <ThemeProvider theme={theme}>
          <Component {...pageProps} />
        </ThemeProvider>
      </StylesProvider>
    </DataContextProvider>
  );
};

export default MyApp;
