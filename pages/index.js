import Head from '../components/head';
import Map from '../components/Map';
import Search from '../components/SearchBox';

const MyApp = () => {
  return (
    <div dir="rtl">
      <Head title="Home" />
      <div className="hero">
        <Map />
        <Search />
      </div>
      <style>
        {`
    .hero {
      width:100%;
      height: 100vh;
      margin: auto;
      overflow: hidden;
      position: relative
    }
  `}
      </style>
    </div>
  );
};

export default MyApp;
