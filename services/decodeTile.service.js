/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
/* eslint-disable no-bitwise */
/* eslint-disable node/no-unsupported-features/node-builtins */
import pako from 'pako';

function decodeTile(data) {
  const keyByteArray = new Int8Array(
    new TextEncoder().encode('eoQF9k7pZ79mntdT4njahddVZmhcfuHLXKsFhUdGsrZv9')
  );
  const bytes = new Int8Array(data);

  if (bytes.length !== 0) {
    const bytesLength = bytes.length;
    const newBytes = new Int8Array(bytesLength);
    let i = 0;
    for (i; i < bytesLength; ++i) {
      newBytes[i] = bytes[i] ^ keyByteArray[i % keyByteArray.length];
    }

    const gzipDecompressed = pako.inflate(new Uint8Array(newBytes));
    data = gzipDecompressed.buffer;
  }

  return data;
}

export default decodeTile;
